/* Felipe Salazar 
 * Mar 3, 2012
 *
 * Finds number of primes in range [2, n]
 * Takes one parameter, n.
 * as per http://programmingpraxis.com/2009/02/19/sieve-of-eratosthenes/
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define kMaxInputNumber 10000000

typedef unsigned long ulong;

char *list = NULL;
size_t num = 0;
unsigned int primeCount = 0;

void print_usage() {
    puts("Usage: sieve [integer]");
    puts("where [integer] is less than 10,000,000");
}

void make_list() {
    assert(list == NULL && num < kMaxInputNumber);
    list = malloc(sizeof(char)*num);
    assert(list != NULL);
}

size_t next_prime(size_t start) {
    for (++start; start <= num; start++) {
        if (list[start] == 0) {
            primeCount++;
            return start;
        }
    }

    return 0;
}

void mark_numbers() {
    size_t n, fac = 1;
    
    while (1) {
        fac = next_prime(fac);
        
        if (fac == 0) return;

        for (n = 2*fac; n <= num; n += fac) {
            list[n] = 1;
        }
    }
}

int main(int argc, char *args[]) {
    if (argc != 2) {
        print_usage();
        return 1;
    } else {
        char *endPtr = args[1];
        num = strtoul(args[1], &endPtr, 0);

        if (endPtr != args[1] && num < kMaxInputNumber) {
            make_list();
            mark_numbers();

            ulong cnt, nm;
            cnt = primeCount; nm = num;
            printf("Found %ld prime numbers in range [2, %ld]\n", cnt, nm); 
        } else {
            print_usage();
        }
    }

    return 0;
}
